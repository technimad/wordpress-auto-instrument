build: traced-wordpress-container

.PHONY: traced-wordpress-container
traced-wordpress-container: traced-wordpress/signalfx-tracing.tar.gz
	docker build --no-cache --progress=plain -t traced-wordpress -f traced-wordpress/Dockerfile traced-wordpress

traced-wordpress/signalfx-tracing.tar.gz:
	curl -s https://api.github.com/repos/signalfx/signalfx-php-tracing/releases/latest \
    | grep browser_download_url \
    | grep signalfx-tracing.tar.gz \
    | cut -d '"' -f 4 \
    | xargs curl -L -o traced-wordpress/signalfx-tracing.tar.gz

.PHONY: clean clean-container
clean: clean-container
	rm traced-wordpress/signalfx-tracing.tar.gz || echo ""

clean-container:
	docker image rm -f traced-wordpress:latest || echo ""